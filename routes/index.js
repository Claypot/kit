'use strict';

var api = require('./api');
var page = require('./page');
var wechat = require('./wechat');

module.exports = function(app){
	// app.get('/socket.io/', function(req, res, next){
	// 	res.header('Access-Control-Allow-Origin', '*');
	// 	next();
	// });
	api(app);
	page(app);
	wechat(app);

	app.get('/test', function(req, res){
		res.send('you name is ' + req.query.name);
	});
}
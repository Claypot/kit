define("url/1.2.0/url", [], function(t, r, e) {
    function s(t) {
        var r = "=",
            e = "&",
            s = {};
        if (!t) return s;
        t = t.replace(/^\?/, "");
        for (var o, i, n, h, u = t.split(e), a = 0, p = u.length; p > a; a++) i = u[a], o = i.indexOf(r), n = decodeURIComponent(i.substring(0, o)) || "", h = decodeURIComponent(i.substring(o + 1)) || "", s.hasOwnProperty(n) ? s[n].push(h) : s[n] = [h];
        return s
    }

    function o(t) {
        var r = [];
        for (var e in t)
            if (t.hasOwnProperty(e)) {
                var s = encodeURIComponent(e),
                    o = t[e];
                if (i(o))
                    for (var n = 0, h = o.length; h > n; n++) r.push(s + "=" + encodeURIComponent(o[n]));
                else r.push(s + "=" + encodeURIComponent(o))
            }
        return (0 === r.length ? "" : "?") + r.join("&")
    }

    function i(t) {
        return "[object Array]" === Object.prototype.toString.call(t)
    }
    var n = /^((?:(\w+:)\/\/)?((\w+):?(\w+)?@)?(([^\/\?:]+)(?:\:(\d+))?))(\/[^\?#]+)?(\?[^#]+)?(#.*)?/,
        h = function(t) {
            var r = n.exec(t);
            this.uri = t, this.origin = r[1], this.protocol = r[2], this.authority, this.username = r[4], this.password = r[5], this.host = r[6] || "", this.hostname = r[7] || "", this.port = r[8] || "", this.path = r[9] || "/", this.search = r[10] || "", this._query = s(this.search), this.hash = r[11] || ""
        };
    h.prototype.getOrigin = function() {
        return this.protocol + "//" + this.getHost()
    }, h.prototype.getHost = function() {
        return this.hostname + (this.port ? ":" + this.port : "")
    }, h.prototype.getParam = function(t) {
        return this._query.hasOwnProperty(t) ? this._query[t][0] : null
    }, h.prototype.getParams = function(t) {
        return this._query.hasOwnProperty(t) ? this._query[t] : []
    }, h.prototype.delParam = function(t) {
        try {
            delete this._query[t], this.search = o(this._query)
        } catch (r) {}
        return this
    }, h.prototype.setParam = function(t, r) {
        return i(r) || (r = [r]), this._query[t] = r, this.search = o(this._query), this
    }, h.prototype.addParam = function(t, r) {
        return i(r) || (r = [r]), this._query[t] = this._query.hasOwnProperty(t) ? this._query[t].concat(r) : r, this.search = o(this._query), this
    }, h.prototype.clearParams = function() {
        return this._query = {}, this.search = o(this._query), this
    }, h.prototype.toString = function() {
        return this.protocol + "//" + (this.username ? this.username + ":" + this.password + "@" : "") + this.hostname + (this.port ? ":" + this.port : "") + this.path + o(this._query) + this.hash
    }, h.verify = function(t) {
        return n.test(t)
    }, e.exports = h
});